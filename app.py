import cv2
import pytesseract
import sys
import mss.tools

print("init")
print(sys.version)

# needed on windows when run locally for some reason?!
pytesseract.pytesseract.tesseract_cmd = r'X:\software\tesseract\tesseract.exe'

with mss.mss() as sct:
    # Get information of monitor 2
    monitor_number = 2
    mon = sct.monitors[monitor_number]

    # 1830 * 190

    # The screen part to capture
    monitor = {
        "top": mon["top"] + 190,  # 100px from the top
        "left": mon["left"] + 1830,  # 100px from the left
        "width": 160,
        "height": 135,
        "mon": monitor_number,
    }

    # Grab the data
    sct_img = sct.grab(monitor)

    # Save to the picture file
    mss.tools.to_png(sct_img.rgb, sct_img.size, output="tmp-monitor-2.png")

print("reading image")
# image = 'image.png'
# image = 'X:\\programming\\ocr-tm-rmc\\image.png'
image = 'tmp-monitor-2.png'
img = cv2.imread(image)

# Adding custom options
print("executing ocr")
custom_config = r'--oem 3 --psm 6'
result = pytesseract.image_to_string(img, config=custom_config)

print("result\n-----------------------------------------\n " + result)

# print("starting to fetch screenshots ")
# stream = r'https://www.twitch.tv/wirtual'
# dir_path = 'out'
# ffmpeg_path = r'X:\software\ffmpeg\ffmpeg-2022-05-26-git-0dcbe1c1aa-essentials_build\bin\ffmpeg.exe'
# counter = 0
# while counter <= 1:
#     os.system( ' ffmpeg -i ' + stream + f' -r 0.5 -frames:v 5 -f image2 {dir_path}/output_%09d.jpg')
#     counter += 1

print("\n-----------------------------------------\nfin")
